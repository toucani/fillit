# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/25 20:28:25 by dkovalch          #+#    #+#              #
#    Updated: 2017/10/29 12:38:36 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

								#################
								#	VARIABLES	#
								#################

NAME			= fillit
NAME_D			= fillit_deb

CC				= gcc

STD_FLAGS		= -x c -std=gnu11
ERR_FLAGS		= -Wall -Werror -Wextra -Wstrict-prototypes

COMPILE_FLAGS	= $(STD_FLAGS) $(ERR_FLAGS) -D NDEBUG
COMPILE_FLAGS_D	= $(STD_FLAGS) $(ERR_FLAGS) -O0 -D DEBUG -g -g3 $(SANITY_FLAGS)

LINKING_FLAGS	= $(ERR_FLAGS)
LINKING_FLAGS_D	= $(ERR_FLAGS) -O0 -g -g3 $(SANITY_FLAGS)

LIBRARIES		= $(LIBFT)
LIBRARIES_D		= $(LIBFT_D)

SANITY_FLAGS	= -fno-omit-frame-pointer \
					-fsanitize=address,shift,null,signed-integer-overflow \
					-fsanitize=vla-bound,bool,enum,undefined

GCC7			:= $(shell command -v gcc-7 2> /dev/null)
ifdef GCC7
CC				= gcc-7
ERR_FLAGS		+= -Wnull-dereference
SANITY_FLAGS	+= -fsanitize=leak,bounds-strict
endif

INCLUDES		= -iquote . -iquote $(LIBFT_FLD)

PRINTF_2_ARGS	= @printf "%-15s %-65s\n"
PRINTF_3_ARGS	= @printf "%-15s %-65s%20s\n"


								#################
								#	LIBRARIES	#
								#################

LIBFT_FLD		= libft

LIBFT			= $(LIBFT_FLD)/libft.a
LIBFT_D			= $(LIBFT_FLD)/libftD.a


								#################
								#	SOURCES		#
								#################

VPATH	= sources


								#################
								#	OBJECTS		#
								#################

OBJECTS_FLD = objects

OBJECTS =\
	$(OBJECTS_FLD)/ft_field.o\
	$(OBJECTS_FLD)/ft_fig.o\
	$(OBJECTS_FLD)/ft_figure.o\
	$(OBJECTS_FLD)/ft_matrix.o\
	$(OBJECTS_FLD)/ft_position.o\
	$(OBJECTS_FLD)/ft_solve.o\
	$(OBJECTS_FLD)/ft_validate.o\
	$(OBJECTS_FLD)/main.o\
	$(OBJECTS_FLD)/ft_tetr.o

OBJECTS_D = $(patsubst %.o, %_D.o, $(OBJECTS))


								#############
								#	RULES	#
								#############

.SILENT :

all : $(NAME)

debug : $(NAME_D)

$(OBJECTS_FLD) :
	mkdir -p $(OBJECTS_FLD)

$(LIBFT) :
	$(PRINTF_2_ARGS) "M͡ak̕i̵ng" "libft"
	make -j -C $(LIBFT_FLD)

$(LIBFT_D) :
	$(PRINTF_2_ARGS) "M͡ak̕i̵ng" "libft debug"
	make debug -j -C $(LIBFT_FLD)

clean :
	rm -rf $(OBJECTS_FLD)
	make clean -C $(LIBFT_FLD)

fclean : clean
	rm -rf $(NAME) $(NAME_D) $(NAME).dSym $(NAME_D).dSym
	make fclean -C $(LIBFT_FLD)

re : fclean all

dre : fclean debug

	#########################
	#	compilation rules	#
	#########################

$(NAME) : $(LIBFT) $(OBJECTS_FLD) $(OBJECTS)
	$(PRINTF_2_ARGS) "Li͢nki͞n҉g" $(NAME)
	$(CC) $(INCLUDES) $(LINKING_FLAGS) $(OBJECTS) $(LIBRARIES) -o $(NAME)

$(NAME_D) : $(LIBFT_D) $(OBJECTS_FLD) $(OBJECTS_D)
	$(PRINTF_3_ARGS) "Li͢nki͞n҉g" $(NAME_D) "wit͠h̀ ̨śa̵nit̷iz̡eŕs̷"
	$(CC) $(INCLUDES) $(LINKING_FLAGS_D) $(OBJECTS_D) $(LIBRARIES_D) -o $(NAME_D)

	#####################
	#	objects rules	#
	#####################

$(OBJECTS_FLD)/%.o : %.c
	$(PRINTF_2_ARGS) "Co̸m͢p͏ili͢ng" $<
	$(CC) $(INCLUDES) $(COMPILE_FLAGS) -c $< -o $@

$(OBJECTS_FLD)/%_D.o : %.c
	$(PRINTF_3_ARGS) "Co̸m͢p͏ili͢ng" $< "with sanitizers"
	$(CC) $(INCLUDES) $(COMPILE_FLAGS_D) -c $< -o $@

# DO NOT DELETE

sources/ft_field.o: fillme.h libft/libft.h /usr/include/unistd.h
sources/ft_field.o: /usr/include/_types.h /usr/include/sys/_types.h
sources/ft_field.o: /usr/include/sys/cdefs.h
sources/ft_field.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_field.o: /usr/include/sys/_posix_availability.h
sources/ft_field.o: /usr/include/machine/_types.h /usr/include/i386/_types.h
sources/ft_field.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_field.o: /usr/include/sys/unistd.h
sources/ft_field.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_field.o: /usr/include/sys/_types/_seek_set.h
sources/ft_field.o: /usr/include/sys/_types/_size_t.h
sources/ft_field.o: /usr/include/_types/_uint64_t.h
sources/ft_field.o: /usr/include/Availability.h
sources/ft_field.o: /usr/include/AvailabilityInternal.h
sources/ft_field.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_field.o: /usr/include/sys/_types/_uid_t.h
sources/ft_field.o: /usr/include/sys/_types/_gid_t.h
sources/ft_field.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_field.o: /usr/include/sys/_types/_off_t.h
sources/ft_field.o: /usr/include/sys/_types/_pid_t.h
sources/ft_field.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_field.o: /usr/include/sys/_types/_null.h /usr/include/sys/select.h
sources/ft_field.o: /usr/include/sys/appleapiopts.h
sources/ft_field.o: /usr/include/sys/_types/_fd_def.h
sources/ft_field.o: /usr/include/sys/_types/_timespec.h
sources/ft_field.o: /usr/include/sys/_types/_timeval.h
sources/ft_field.o: /usr/include/sys/_types/_time_t.h
sources/ft_field.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_field.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_field.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_field.o: /usr/include/sys/_types/_fd_set.h
sources/ft_field.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_field.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_field.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_field.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_field.o: /usr/include/sys/_select.h
sources/ft_field.o: /usr/include/sys/_types/_dev_t.h
sources/ft_field.o: /usr/include/sys/_types/_mode_t.h
sources/ft_field.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_field.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_field.o: /usr/include/sys/_types/_offsetof.h
sources/ft_field.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_field.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_field.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_field.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/ft_field.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/ft_field.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/ft_field.o: /usr/include/i386/signal.h
sources/ft_field.o: /usr/include/machine/_mcontext.h
sources/ft_field.o: /usr/include/i386/_mcontext.h
sources/ft_field.o: /usr/include/mach/i386/_structs.h
sources/ft_field.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_field.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_field.o: /usr/include/sys/_types/_ucontext.h
sources/ft_field.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_field.o: /usr/include/sys/_types/_int8_t.h
sources/ft_field.o: /usr/include/sys/_types/_int16_t.h
sources/ft_field.o: /usr/include/sys/_types/_int32_t.h
sources/ft_field.o: /usr/include/sys/_types/_int64_t.h
sources/ft_field.o: /usr/include/_types/_uint8_t.h
sources/ft_field.o: /usr/include/_types/_uint16_t.h
sources/ft_field.o: /usr/include/_types/_uint32_t.h
sources/ft_field.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_field.o: /usr/include/_types/_intmax_t.h
sources/ft_field.o: /usr/include/_types/_uintmax_t.h
sources/ft_field.o: /usr/include/machine/endian.h /usr/include/i386/endian.h
sources/ft_field.o: /usr/include/sys/_endian.h
sources/ft_field.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_field.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_field.o: /usr/include/alloca.h
sources/ft_field.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_field.o: /usr/include/sys/_types/_rune_t.h
sources/ft_field.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_field.o: /usr/include/inttypes.h libft/get_next_line.h
sources/ft_field.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/ft_field.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_field.o: /usr/include/secure/_common.h /usr/include/fcntl.h
sources/ft_field.o: /usr/include/sys/fcntl.h
sources/ft_field.o: /usr/include/sys/_types/_o_sync.h
sources/ft_field.o: /usr/include/sys/_types/_o_dsync.h
sources/ft_field.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_field.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_field.o: /usr/include/sys/stat.h
sources/ft_field.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_field.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_field.o: /usr/include/sys/_types/_ino_t.h
sources/ft_field.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_field.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_field.o: /usr/include/sys/types.h
sources/ft_field.o: /usr/include/sys/_types/_in_addr_t.h
sources/ft_field.o: /usr/include/sys/_types/_in_port_t.h
sources/ft_field.o: /usr/include/sys/_types/_key_t.h
sources/ft_field.o: /usr/include/sys/_types/_clock_t.h
sources/ft_field.o: /usr/include/sys/_types/_errno_t.h
sources/ft_field.o: /usr/include/sys/_pthread/_pthread_cond_t.h
sources/ft_field.o: /usr/include/sys/_pthread/_pthread_condattr_t.h
sources/ft_field.o: /usr/include/sys/_pthread/_pthread_mutex_t.h
sources/ft_field.o: /usr/include/sys/_pthread/_pthread_mutexattr_t.h
sources/ft_field.o: /usr/include/sys/_pthread/_pthread_once_t.h
sources/ft_field.o: /usr/include/sys/_pthread/_pthread_rwlock_t.h
sources/ft_field.o: /usr/include/sys/_pthread/_pthread_rwlockattr_t.h
sources/ft_field.o: /usr/include/sys/_pthread/_pthread_t.h
sources/ft_field.o: /usr/include/sys/_pthread/_pthread_key_t.h
sources/ft_field.o: /usr/include/sys/_types/_fsblkcnt_t.h
sources/ft_field.o: /usr/include/sys/_types/_fsfilcnt_t.h
sources/ft_fig.o: fillme.h libft/libft.h /usr/include/unistd.h
sources/ft_fig.o: /usr/include/_types.h /usr/include/sys/_types.h
sources/ft_fig.o: /usr/include/sys/cdefs.h
sources/ft_fig.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_fig.o: /usr/include/sys/_posix_availability.h
sources/ft_fig.o: /usr/include/machine/_types.h /usr/include/i386/_types.h
sources/ft_fig.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_fig.o: /usr/include/sys/unistd.h
sources/ft_fig.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_fig.o: /usr/include/sys/_types/_seek_set.h
sources/ft_fig.o: /usr/include/sys/_types/_size_t.h
sources/ft_fig.o: /usr/include/_types/_uint64_t.h /usr/include/Availability.h
sources/ft_fig.o: /usr/include/AvailabilityInternal.h
sources/ft_fig.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_fig.o: /usr/include/sys/_types/_uid_t.h
sources/ft_fig.o: /usr/include/sys/_types/_gid_t.h
sources/ft_fig.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_fig.o: /usr/include/sys/_types/_off_t.h
sources/ft_fig.o: /usr/include/sys/_types/_pid_t.h
sources/ft_fig.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_fig.o: /usr/include/sys/_types/_null.h /usr/include/sys/select.h
sources/ft_fig.o: /usr/include/sys/appleapiopts.h
sources/ft_fig.o: /usr/include/sys/_types/_fd_def.h
sources/ft_fig.o: /usr/include/sys/_types/_timespec.h
sources/ft_fig.o: /usr/include/sys/_types/_timeval.h
sources/ft_fig.o: /usr/include/sys/_types/_time_t.h
sources/ft_fig.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_fig.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_fig.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_fig.o: /usr/include/sys/_types/_fd_set.h
sources/ft_fig.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_fig.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_fig.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_fig.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_fig.o: /usr/include/sys/_select.h /usr/include/sys/_types/_dev_t.h
sources/ft_fig.o: /usr/include/sys/_types/_mode_t.h
sources/ft_fig.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_fig.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_fig.o: /usr/include/sys/_types/_offsetof.h
sources/ft_fig.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_fig.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_fig.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_fig.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/ft_fig.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/ft_fig.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/ft_fig.o: /usr/include/i386/signal.h /usr/include/machine/_mcontext.h
sources/ft_fig.o: /usr/include/i386/_mcontext.h
sources/ft_fig.o: /usr/include/mach/i386/_structs.h
sources/ft_fig.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_fig.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_fig.o: /usr/include/sys/_types/_ucontext.h
sources/ft_fig.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_fig.o: /usr/include/sys/_types/_int8_t.h
sources/ft_fig.o: /usr/include/sys/_types/_int16_t.h
sources/ft_fig.o: /usr/include/sys/_types/_int32_t.h
sources/ft_fig.o: /usr/include/sys/_types/_int64_t.h
sources/ft_fig.o: /usr/include/_types/_uint8_t.h
sources/ft_fig.o: /usr/include/_types/_uint16_t.h
sources/ft_fig.o: /usr/include/_types/_uint32_t.h
sources/ft_fig.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_fig.o: /usr/include/_types/_intmax_t.h
sources/ft_fig.o: /usr/include/_types/_uintmax_t.h
sources/ft_fig.o: /usr/include/machine/endian.h /usr/include/i386/endian.h
sources/ft_fig.o: /usr/include/sys/_endian.h
sources/ft_fig.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_fig.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_fig.o: /usr/include/alloca.h /usr/include/sys/_types/_ct_rune_t.h
sources/ft_fig.o: /usr/include/sys/_types/_rune_t.h
sources/ft_fig.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_fig.o: /usr/include/inttypes.h libft/get_next_line.h
sources/ft_fig.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/ft_fig.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_fig.o: /usr/include/secure/_common.h /usr/include/fcntl.h
sources/ft_fig.o: /usr/include/sys/fcntl.h /usr/include/sys/_types/_o_sync.h
sources/ft_fig.o: /usr/include/sys/_types/_o_dsync.h
sources/ft_fig.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_fig.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_fig.o: /usr/include/sys/stat.h /usr/include/sys/_types/_blkcnt_t.h
sources/ft_fig.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_fig.o: /usr/include/sys/_types/_ino_t.h
sources/ft_fig.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_fig.o: /usr/include/sys/_types/_nlink_t.h /usr/include/sys/types.h
sources/ft_fig.o: /usr/include/sys/_types/_in_addr_t.h
sources/ft_fig.o: /usr/include/sys/_types/_in_port_t.h
sources/ft_fig.o: /usr/include/sys/_types/_key_t.h
sources/ft_fig.o: /usr/include/sys/_types/_clock_t.h
sources/ft_fig.o: /usr/include/sys/_types/_errno_t.h
sources/ft_fig.o: /usr/include/sys/_pthread/_pthread_cond_t.h
sources/ft_fig.o: /usr/include/sys/_pthread/_pthread_condattr_t.h
sources/ft_fig.o: /usr/include/sys/_pthread/_pthread_mutex_t.h
sources/ft_fig.o: /usr/include/sys/_pthread/_pthread_mutexattr_t.h
sources/ft_fig.o: /usr/include/sys/_pthread/_pthread_once_t.h
sources/ft_fig.o: /usr/include/sys/_pthread/_pthread_rwlock_t.h
sources/ft_fig.o: /usr/include/sys/_pthread/_pthread_rwlockattr_t.h
sources/ft_fig.o: /usr/include/sys/_pthread/_pthread_t.h
sources/ft_fig.o: /usr/include/sys/_pthread/_pthread_key_t.h
sources/ft_fig.o: /usr/include/sys/_types/_fsblkcnt_t.h
sources/ft_fig.o: /usr/include/sys/_types/_fsfilcnt_t.h /usr/include/limits.h
sources/ft_fig.o: /usr/include/machine/limits.h /usr/include/i386/limits.h
sources/ft_fig.o: /usr/include/i386/_limits.h /usr/include/sys/syslimits.h
sources/ft_figure.o: fillme.h libft/libft.h /usr/include/unistd.h
sources/ft_figure.o: /usr/include/_types.h /usr/include/sys/_types.h
sources/ft_figure.o: /usr/include/sys/cdefs.h
sources/ft_figure.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_figure.o: /usr/include/sys/_posix_availability.h
sources/ft_figure.o: /usr/include/machine/_types.h /usr/include/i386/_types.h
sources/ft_figure.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_figure.o: /usr/include/sys/unistd.h
sources/ft_figure.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_figure.o: /usr/include/sys/_types/_seek_set.h
sources/ft_figure.o: /usr/include/sys/_types/_size_t.h
sources/ft_figure.o: /usr/include/_types/_uint64_t.h
sources/ft_figure.o: /usr/include/Availability.h
sources/ft_figure.o: /usr/include/AvailabilityInternal.h
sources/ft_figure.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_figure.o: /usr/include/sys/_types/_uid_t.h
sources/ft_figure.o: /usr/include/sys/_types/_gid_t.h
sources/ft_figure.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_figure.o: /usr/include/sys/_types/_off_t.h
sources/ft_figure.o: /usr/include/sys/_types/_pid_t.h
sources/ft_figure.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_figure.o: /usr/include/sys/_types/_null.h
sources/ft_figure.o: /usr/include/sys/select.h
sources/ft_figure.o: /usr/include/sys/appleapiopts.h
sources/ft_figure.o: /usr/include/sys/_types/_fd_def.h
sources/ft_figure.o: /usr/include/sys/_types/_timespec.h
sources/ft_figure.o: /usr/include/sys/_types/_timeval.h
sources/ft_figure.o: /usr/include/sys/_types/_time_t.h
sources/ft_figure.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_figure.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_figure.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_figure.o: /usr/include/sys/_types/_fd_set.h
sources/ft_figure.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_figure.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_figure.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_figure.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_figure.o: /usr/include/sys/_select.h
sources/ft_figure.o: /usr/include/sys/_types/_dev_t.h
sources/ft_figure.o: /usr/include/sys/_types/_mode_t.h
sources/ft_figure.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_figure.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_figure.o: /usr/include/sys/_types/_offsetof.h
sources/ft_figure.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_figure.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_figure.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_figure.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/ft_figure.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/ft_figure.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/ft_figure.o: /usr/include/i386/signal.h
sources/ft_figure.o: /usr/include/machine/_mcontext.h
sources/ft_figure.o: /usr/include/i386/_mcontext.h
sources/ft_figure.o: /usr/include/mach/i386/_structs.h
sources/ft_figure.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_figure.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_figure.o: /usr/include/sys/_types/_ucontext.h
sources/ft_figure.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_figure.o: /usr/include/sys/_types/_int8_t.h
sources/ft_figure.o: /usr/include/sys/_types/_int16_t.h
sources/ft_figure.o: /usr/include/sys/_types/_int32_t.h
sources/ft_figure.o: /usr/include/sys/_types/_int64_t.h
sources/ft_figure.o: /usr/include/_types/_uint8_t.h
sources/ft_figure.o: /usr/include/_types/_uint16_t.h
sources/ft_figure.o: /usr/include/_types/_uint32_t.h
sources/ft_figure.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_figure.o: /usr/include/_types/_intmax_t.h
sources/ft_figure.o: /usr/include/_types/_uintmax_t.h
sources/ft_figure.o: /usr/include/machine/endian.h /usr/include/i386/endian.h
sources/ft_figure.o: /usr/include/sys/_endian.h
sources/ft_figure.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_figure.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_figure.o: /usr/include/alloca.h
sources/ft_figure.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_figure.o: /usr/include/sys/_types/_rune_t.h
sources/ft_figure.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_figure.o: /usr/include/inttypes.h libft/get_next_line.h
sources/ft_figure.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/ft_figure.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_figure.o: /usr/include/secure/_common.h /usr/include/fcntl.h
sources/ft_figure.o: /usr/include/sys/fcntl.h
sources/ft_figure.o: /usr/include/sys/_types/_o_sync.h
sources/ft_figure.o: /usr/include/sys/_types/_o_dsync.h
sources/ft_figure.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_figure.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_figure.o: /usr/include/sys/stat.h
sources/ft_figure.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_figure.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_figure.o: /usr/include/sys/_types/_ino_t.h
sources/ft_figure.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_figure.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_figure.o: /usr/include/sys/types.h
sources/ft_figure.o: /usr/include/sys/_types/_in_addr_t.h
sources/ft_figure.o: /usr/include/sys/_types/_in_port_t.h
sources/ft_figure.o: /usr/include/sys/_types/_key_t.h
sources/ft_figure.o: /usr/include/sys/_types/_clock_t.h
sources/ft_figure.o: /usr/include/sys/_types/_errno_t.h
sources/ft_figure.o: /usr/include/sys/_pthread/_pthread_cond_t.h
sources/ft_figure.o: /usr/include/sys/_pthread/_pthread_condattr_t.h
sources/ft_figure.o: /usr/include/sys/_pthread/_pthread_mutex_t.h
sources/ft_figure.o: /usr/include/sys/_pthread/_pthread_mutexattr_t.h
sources/ft_figure.o: /usr/include/sys/_pthread/_pthread_once_t.h
sources/ft_figure.o: /usr/include/sys/_pthread/_pthread_rwlock_t.h
sources/ft_figure.o: /usr/include/sys/_pthread/_pthread_rwlockattr_t.h
sources/ft_figure.o: /usr/include/sys/_pthread/_pthread_t.h
sources/ft_figure.o: /usr/include/sys/_pthread/_pthread_key_t.h
sources/ft_figure.o: /usr/include/sys/_types/_fsblkcnt_t.h
sources/ft_figure.o: /usr/include/sys/_types/_fsfilcnt_t.h
sources/ft_find_position.o: fillme.h libft/libft.h /usr/include/unistd.h
sources/ft_find_position.o: /usr/include/_types.h /usr/include/sys/_types.h
sources/ft_find_position.o: /usr/include/sys/cdefs.h
sources/ft_find_position.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_find_position.o: /usr/include/sys/_posix_availability.h
sources/ft_find_position.o: /usr/include/machine/_types.h
sources/ft_find_position.o: /usr/include/i386/_types.h
sources/ft_find_position.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_find_position.o: /usr/include/sys/unistd.h
sources/ft_find_position.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_find_position.o: /usr/include/sys/_types/_seek_set.h
sources/ft_find_position.o: /usr/include/sys/_types/_size_t.h
sources/ft_find_position.o: /usr/include/_types/_uint64_t.h
sources/ft_find_position.o: /usr/include/Availability.h
sources/ft_find_position.o: /usr/include/AvailabilityInternal.h
sources/ft_find_position.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_uid_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_gid_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_off_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_pid_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_null.h
sources/ft_find_position.o: /usr/include/sys/select.h
sources/ft_find_position.o: /usr/include/sys/appleapiopts.h
sources/ft_find_position.o: /usr/include/sys/_types/_fd_def.h
sources/ft_find_position.o: /usr/include/sys/_types/_timespec.h
sources/ft_find_position.o: /usr/include/sys/_types/_timeval.h
sources/ft_find_position.o: /usr/include/sys/_types/_time_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_find_position.o: /usr/include/sys/_types/_fd_set.h
sources/ft_find_position.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_find_position.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_find_position.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_find_position.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_find_position.o: /usr/include/sys/_select.h
sources/ft_find_position.o: /usr/include/sys/_types/_dev_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_mode_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_find_position.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_find_position.o: /usr/include/sys/_types/_offsetof.h
sources/ft_find_position.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_wint_t.h
sources/ft_find_position.o: /usr/include/stdlib.h /usr/include/sys/wait.h
sources/ft_find_position.o: /usr/include/sys/_types/_id_t.h
sources/ft_find_position.o: /usr/include/sys/signal.h
sources/ft_find_position.o: /usr/include/machine/signal.h
sources/ft_find_position.o: /usr/include/i386/signal.h
sources/ft_find_position.o: /usr/include/machine/_mcontext.h
sources/ft_find_position.o: /usr/include/i386/_mcontext.h
sources/ft_find_position.o: /usr/include/mach/i386/_structs.h
sources/ft_find_position.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_find_position.o: /usr/include/sys/_types/_ucontext.h
sources/ft_find_position.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_find_position.o: /usr/include/sys/_types/_int8_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_int16_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_int32_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_int64_t.h
sources/ft_find_position.o: /usr/include/_types/_uint8_t.h
sources/ft_find_position.o: /usr/include/_types/_uint16_t.h
sources/ft_find_position.o: /usr/include/_types/_uint32_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_find_position.o: /usr/include/_types/_intmax_t.h
sources/ft_find_position.o: /usr/include/_types/_uintmax_t.h
sources/ft_find_position.o: /usr/include/machine/endian.h
sources/ft_find_position.o: /usr/include/i386/endian.h
sources/ft_find_position.o: /usr/include/sys/_endian.h
sources/ft_find_position.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_find_position.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_find_position.o: /usr/include/alloca.h
sources/ft_find_position.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_rune_t.h
sources/ft_find_position.o: /usr/include/machine/types.h
sources/ft_find_position.o: /usr/include/i386/types.h /usr/include/inttypes.h
sources/ft_find_position.o: libft/get_next_line.h /usr/include/stdio.h
sources/ft_find_position.o: /usr/include/sys/_types/_va_list.h
sources/ft_find_position.o: /usr/include/sys/stdio.h
sources/ft_find_position.o: /usr/include/secure/_stdio.h
sources/ft_find_position.o: /usr/include/secure/_common.h
sources/ft_find_position.o: /usr/include/fcntl.h /usr/include/sys/fcntl.h
sources/ft_find_position.o: /usr/include/sys/_types/_o_sync.h
sources/ft_find_position.o: /usr/include/sys/_types/_o_dsync.h
sources/ft_find_position.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_find_position.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_find_position.o: /usr/include/sys/stat.h
sources/ft_find_position.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_ino_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_find_position.o: /usr/include/sys/types.h
sources/ft_find_position.o: /usr/include/sys/_types/_in_addr_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_in_port_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_key_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_clock_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_errno_t.h
sources/ft_find_position.o: /usr/include/sys/_pthread/_pthread_cond_t.h
sources/ft_find_position.o: /usr/include/sys/_pthread/_pthread_condattr_t.h
sources/ft_find_position.o: /usr/include/sys/_pthread/_pthread_mutex_t.h
sources/ft_find_position.o: /usr/include/sys/_pthread/_pthread_mutexattr_t.h
sources/ft_find_position.o: /usr/include/sys/_pthread/_pthread_once_t.h
sources/ft_find_position.o: /usr/include/sys/_pthread/_pthread_rwlock_t.h
sources/ft_find_position.o: /usr/include/sys/_pthread/_pthread_rwlockattr_t.h
sources/ft_find_position.o: /usr/include/sys/_pthread/_pthread_t.h
sources/ft_find_position.o: /usr/include/sys/_pthread/_pthread_key_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_fsblkcnt_t.h
sources/ft_find_position.o: /usr/include/sys/_types/_fsfilcnt_t.h
sources/ft_matrix.o: fillme.h libft/libft.h /usr/include/unistd.h
sources/ft_matrix.o: /usr/include/_types.h /usr/include/sys/_types.h
sources/ft_matrix.o: /usr/include/sys/cdefs.h
sources/ft_matrix.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_matrix.o: /usr/include/sys/_posix_availability.h
sources/ft_matrix.o: /usr/include/machine/_types.h /usr/include/i386/_types.h
sources/ft_matrix.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_matrix.o: /usr/include/sys/unistd.h
sources/ft_matrix.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_matrix.o: /usr/include/sys/_types/_seek_set.h
sources/ft_matrix.o: /usr/include/sys/_types/_size_t.h
sources/ft_matrix.o: /usr/include/_types/_uint64_t.h
sources/ft_matrix.o: /usr/include/Availability.h
sources/ft_matrix.o: /usr/include/AvailabilityInternal.h
sources/ft_matrix.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_uid_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_gid_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_off_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_pid_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_null.h
sources/ft_matrix.o: /usr/include/sys/select.h
sources/ft_matrix.o: /usr/include/sys/appleapiopts.h
sources/ft_matrix.o: /usr/include/sys/_types/_fd_def.h
sources/ft_matrix.o: /usr/include/sys/_types/_timespec.h
sources/ft_matrix.o: /usr/include/sys/_types/_timeval.h
sources/ft_matrix.o: /usr/include/sys/_types/_time_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_matrix.o: /usr/include/sys/_types/_fd_set.h
sources/ft_matrix.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_matrix.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_matrix.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_matrix.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_matrix.o: /usr/include/sys/_select.h
sources/ft_matrix.o: /usr/include/sys/_types/_dev_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_mode_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_matrix.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_matrix.o: /usr/include/sys/_types/_offsetof.h
sources/ft_matrix.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/ft_matrix.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/ft_matrix.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/ft_matrix.o: /usr/include/i386/signal.h
sources/ft_matrix.o: /usr/include/machine/_mcontext.h
sources/ft_matrix.o: /usr/include/i386/_mcontext.h
sources/ft_matrix.o: /usr/include/mach/i386/_structs.h
sources/ft_matrix.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_matrix.o: /usr/include/sys/_types/_ucontext.h
sources/ft_matrix.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_matrix.o: /usr/include/sys/_types/_int8_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_int16_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_int32_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_int64_t.h
sources/ft_matrix.o: /usr/include/_types/_uint8_t.h
sources/ft_matrix.o: /usr/include/_types/_uint16_t.h
sources/ft_matrix.o: /usr/include/_types/_uint32_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_matrix.o: /usr/include/_types/_intmax_t.h
sources/ft_matrix.o: /usr/include/_types/_uintmax_t.h
sources/ft_matrix.o: /usr/include/machine/endian.h /usr/include/i386/endian.h
sources/ft_matrix.o: /usr/include/sys/_endian.h
sources/ft_matrix.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_matrix.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_matrix.o: /usr/include/alloca.h
sources/ft_matrix.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_rune_t.h
sources/ft_matrix.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_matrix.o: /usr/include/inttypes.h libft/get_next_line.h
sources/ft_matrix.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/ft_matrix.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_matrix.o: /usr/include/secure/_common.h /usr/include/fcntl.h
sources/ft_matrix.o: /usr/include/sys/fcntl.h
sources/ft_matrix.o: /usr/include/sys/_types/_o_sync.h
sources/ft_matrix.o: /usr/include/sys/_types/_o_dsync.h
sources/ft_matrix.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_matrix.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_matrix.o: /usr/include/sys/stat.h
sources/ft_matrix.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_ino_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_matrix.o: /usr/include/sys/types.h
sources/ft_matrix.o: /usr/include/sys/_types/_in_addr_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_in_port_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_key_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_clock_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_errno_t.h
sources/ft_matrix.o: /usr/include/sys/_pthread/_pthread_cond_t.h
sources/ft_matrix.o: /usr/include/sys/_pthread/_pthread_condattr_t.h
sources/ft_matrix.o: /usr/include/sys/_pthread/_pthread_mutex_t.h
sources/ft_matrix.o: /usr/include/sys/_pthread/_pthread_mutexattr_t.h
sources/ft_matrix.o: /usr/include/sys/_pthread/_pthread_once_t.h
sources/ft_matrix.o: /usr/include/sys/_pthread/_pthread_rwlock_t.h
sources/ft_matrix.o: /usr/include/sys/_pthread/_pthread_rwlockattr_t.h
sources/ft_matrix.o: /usr/include/sys/_pthread/_pthread_t.h
sources/ft_matrix.o: /usr/include/sys/_pthread/_pthread_key_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_fsblkcnt_t.h
sources/ft_matrix.o: /usr/include/sys/_types/_fsfilcnt_t.h
sources/ft_matrix.o: /usr/include/limits.h /usr/include/machine/limits.h
sources/ft_matrix.o: /usr/include/i386/limits.h /usr/include/i386/_limits.h
sources/ft_matrix.o: /usr/include/sys/syslimits.h
sources/ft_position.o: fillme.h libft/libft.h /usr/include/unistd.h
sources/ft_position.o: /usr/include/_types.h /usr/include/sys/_types.h
sources/ft_position.o: /usr/include/sys/cdefs.h
sources/ft_position.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_position.o: /usr/include/sys/_posix_availability.h
sources/ft_position.o: /usr/include/machine/_types.h
sources/ft_position.o: /usr/include/i386/_types.h
sources/ft_position.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_position.o: /usr/include/sys/unistd.h
sources/ft_position.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_position.o: /usr/include/sys/_types/_seek_set.h
sources/ft_position.o: /usr/include/sys/_types/_size_t.h
sources/ft_position.o: /usr/include/_types/_uint64_t.h
sources/ft_position.o: /usr/include/Availability.h
sources/ft_position.o: /usr/include/AvailabilityInternal.h
sources/ft_position.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_position.o: /usr/include/sys/_types/_uid_t.h
sources/ft_position.o: /usr/include/sys/_types/_gid_t.h
sources/ft_position.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_position.o: /usr/include/sys/_types/_off_t.h
sources/ft_position.o: /usr/include/sys/_types/_pid_t.h
sources/ft_position.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_position.o: /usr/include/sys/_types/_null.h
sources/ft_position.o: /usr/include/sys/select.h
sources/ft_position.o: /usr/include/sys/appleapiopts.h
sources/ft_position.o: /usr/include/sys/_types/_fd_def.h
sources/ft_position.o: /usr/include/sys/_types/_timespec.h
sources/ft_position.o: /usr/include/sys/_types/_timeval.h
sources/ft_position.o: /usr/include/sys/_types/_time_t.h
sources/ft_position.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_position.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_position.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_position.o: /usr/include/sys/_types/_fd_set.h
sources/ft_position.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_position.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_position.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_position.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_position.o: /usr/include/sys/_select.h
sources/ft_position.o: /usr/include/sys/_types/_dev_t.h
sources/ft_position.o: /usr/include/sys/_types/_mode_t.h
sources/ft_position.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_position.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_position.o: /usr/include/sys/_types/_offsetof.h
sources/ft_position.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_position.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_position.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_position.o: /usr/include/sys/_types/_wint_t.h
sources/ft_position.o: /usr/include/stdlib.h /usr/include/sys/wait.h
sources/ft_position.o: /usr/include/sys/_types/_id_t.h
sources/ft_position.o: /usr/include/sys/signal.h
sources/ft_position.o: /usr/include/machine/signal.h
sources/ft_position.o: /usr/include/i386/signal.h
sources/ft_position.o: /usr/include/machine/_mcontext.h
sources/ft_position.o: /usr/include/i386/_mcontext.h
sources/ft_position.o: /usr/include/mach/i386/_structs.h
sources/ft_position.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_position.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_position.o: /usr/include/sys/_types/_ucontext.h
sources/ft_position.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_position.o: /usr/include/sys/_types/_int8_t.h
sources/ft_position.o: /usr/include/sys/_types/_int16_t.h
sources/ft_position.o: /usr/include/sys/_types/_int32_t.h
sources/ft_position.o: /usr/include/sys/_types/_int64_t.h
sources/ft_position.o: /usr/include/_types/_uint8_t.h
sources/ft_position.o: /usr/include/_types/_uint16_t.h
sources/ft_position.o: /usr/include/_types/_uint32_t.h
sources/ft_position.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_position.o: /usr/include/_types/_intmax_t.h
sources/ft_position.o: /usr/include/_types/_uintmax_t.h
sources/ft_position.o: /usr/include/machine/endian.h
sources/ft_position.o: /usr/include/i386/endian.h /usr/include/sys/_endian.h
sources/ft_position.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_position.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_position.o: /usr/include/alloca.h
sources/ft_position.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_position.o: /usr/include/sys/_types/_rune_t.h
sources/ft_position.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_position.o: /usr/include/inttypes.h libft/get_next_line.h
sources/ft_position.o: /usr/include/stdio.h
sources/ft_position.o: /usr/include/sys/_types/_va_list.h
sources/ft_position.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_position.o: /usr/include/secure/_common.h /usr/include/fcntl.h
sources/ft_position.o: /usr/include/sys/fcntl.h
sources/ft_position.o: /usr/include/sys/_types/_o_sync.h
sources/ft_position.o: /usr/include/sys/_types/_o_dsync.h
sources/ft_position.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_position.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_position.o: /usr/include/sys/stat.h
sources/ft_position.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_position.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_position.o: /usr/include/sys/_types/_ino_t.h
sources/ft_position.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_position.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_position.o: /usr/include/sys/types.h
sources/ft_position.o: /usr/include/sys/_types/_in_addr_t.h
sources/ft_position.o: /usr/include/sys/_types/_in_port_t.h
sources/ft_position.o: /usr/include/sys/_types/_key_t.h
sources/ft_position.o: /usr/include/sys/_types/_clock_t.h
sources/ft_position.o: /usr/include/sys/_types/_errno_t.h
sources/ft_position.o: /usr/include/sys/_pthread/_pthread_cond_t.h
sources/ft_position.o: /usr/include/sys/_pthread/_pthread_condattr_t.h
sources/ft_position.o: /usr/include/sys/_pthread/_pthread_mutex_t.h
sources/ft_position.o: /usr/include/sys/_pthread/_pthread_mutexattr_t.h
sources/ft_position.o: /usr/include/sys/_pthread/_pthread_once_t.h
sources/ft_position.o: /usr/include/sys/_pthread/_pthread_rwlock_t.h
sources/ft_position.o: /usr/include/sys/_pthread/_pthread_rwlockattr_t.h
sources/ft_position.o: /usr/include/sys/_pthread/_pthread_t.h
sources/ft_position.o: /usr/include/sys/_pthread/_pthread_key_t.h
sources/ft_position.o: /usr/include/sys/_types/_fsblkcnt_t.h
sources/ft_position.o: /usr/include/sys/_types/_fsfilcnt_t.h
sources/ft_solve.o: fillme.h libft/libft.h /usr/include/unistd.h
sources/ft_solve.o: /usr/include/_types.h /usr/include/sys/_types.h
sources/ft_solve.o: /usr/include/sys/cdefs.h
sources/ft_solve.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_solve.o: /usr/include/sys/_posix_availability.h
sources/ft_solve.o: /usr/include/machine/_types.h /usr/include/i386/_types.h
sources/ft_solve.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_solve.o: /usr/include/sys/unistd.h
sources/ft_solve.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_solve.o: /usr/include/sys/_types/_seek_set.h
sources/ft_solve.o: /usr/include/sys/_types/_size_t.h
sources/ft_solve.o: /usr/include/_types/_uint64_t.h
sources/ft_solve.o: /usr/include/Availability.h
sources/ft_solve.o: /usr/include/AvailabilityInternal.h
sources/ft_solve.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_solve.o: /usr/include/sys/_types/_uid_t.h
sources/ft_solve.o: /usr/include/sys/_types/_gid_t.h
sources/ft_solve.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_solve.o: /usr/include/sys/_types/_off_t.h
sources/ft_solve.o: /usr/include/sys/_types/_pid_t.h
sources/ft_solve.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_solve.o: /usr/include/sys/_types/_null.h /usr/include/sys/select.h
sources/ft_solve.o: /usr/include/sys/appleapiopts.h
sources/ft_solve.o: /usr/include/sys/_types/_fd_def.h
sources/ft_solve.o: /usr/include/sys/_types/_timespec.h
sources/ft_solve.o: /usr/include/sys/_types/_timeval.h
sources/ft_solve.o: /usr/include/sys/_types/_time_t.h
sources/ft_solve.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_solve.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_solve.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_solve.o: /usr/include/sys/_types/_fd_set.h
sources/ft_solve.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_solve.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_solve.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_solve.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_solve.o: /usr/include/sys/_select.h
sources/ft_solve.o: /usr/include/sys/_types/_dev_t.h
sources/ft_solve.o: /usr/include/sys/_types/_mode_t.h
sources/ft_solve.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_solve.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_solve.o: /usr/include/sys/_types/_offsetof.h
sources/ft_solve.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_solve.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_solve.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_solve.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/ft_solve.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/ft_solve.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/ft_solve.o: /usr/include/i386/signal.h
sources/ft_solve.o: /usr/include/machine/_mcontext.h
sources/ft_solve.o: /usr/include/i386/_mcontext.h
sources/ft_solve.o: /usr/include/mach/i386/_structs.h
sources/ft_solve.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_solve.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_solve.o: /usr/include/sys/_types/_ucontext.h
sources/ft_solve.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_solve.o: /usr/include/sys/_types/_int8_t.h
sources/ft_solve.o: /usr/include/sys/_types/_int16_t.h
sources/ft_solve.o: /usr/include/sys/_types/_int32_t.h
sources/ft_solve.o: /usr/include/sys/_types/_int64_t.h
sources/ft_solve.o: /usr/include/_types/_uint8_t.h
sources/ft_solve.o: /usr/include/_types/_uint16_t.h
sources/ft_solve.o: /usr/include/_types/_uint32_t.h
sources/ft_solve.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_solve.o: /usr/include/_types/_intmax_t.h
sources/ft_solve.o: /usr/include/_types/_uintmax_t.h
sources/ft_solve.o: /usr/include/machine/endian.h /usr/include/i386/endian.h
sources/ft_solve.o: /usr/include/sys/_endian.h
sources/ft_solve.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_solve.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_solve.o: /usr/include/alloca.h
sources/ft_solve.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_solve.o: /usr/include/sys/_types/_rune_t.h
sources/ft_solve.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_solve.o: /usr/include/inttypes.h libft/get_next_line.h
sources/ft_solve.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/ft_solve.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_solve.o: /usr/include/secure/_common.h /usr/include/fcntl.h
sources/ft_solve.o: /usr/include/sys/fcntl.h
sources/ft_solve.o: /usr/include/sys/_types/_o_sync.h
sources/ft_solve.o: /usr/include/sys/_types/_o_dsync.h
sources/ft_solve.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_solve.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_solve.o: /usr/include/sys/stat.h
sources/ft_solve.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_solve.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_solve.o: /usr/include/sys/_types/_ino_t.h
sources/ft_solve.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_solve.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_solve.o: /usr/include/sys/types.h
sources/ft_solve.o: /usr/include/sys/_types/_in_addr_t.h
sources/ft_solve.o: /usr/include/sys/_types/_in_port_t.h
sources/ft_solve.o: /usr/include/sys/_types/_key_t.h
sources/ft_solve.o: /usr/include/sys/_types/_clock_t.h
sources/ft_solve.o: /usr/include/sys/_types/_errno_t.h
sources/ft_solve.o: /usr/include/sys/_pthread/_pthread_cond_t.h
sources/ft_solve.o: /usr/include/sys/_pthread/_pthread_condattr_t.h
sources/ft_solve.o: /usr/include/sys/_pthread/_pthread_mutex_t.h
sources/ft_solve.o: /usr/include/sys/_pthread/_pthread_mutexattr_t.h
sources/ft_solve.o: /usr/include/sys/_pthread/_pthread_once_t.h
sources/ft_solve.o: /usr/include/sys/_pthread/_pthread_rwlock_t.h
sources/ft_solve.o: /usr/include/sys/_pthread/_pthread_rwlockattr_t.h
sources/ft_solve.o: /usr/include/sys/_pthread/_pthread_t.h
sources/ft_solve.o: /usr/include/sys/_pthread/_pthread_key_t.h
sources/ft_solve.o: /usr/include/sys/_types/_fsblkcnt_t.h
sources/ft_solve.o: /usr/include/sys/_types/_fsfilcnt_t.h
sources/ft_tetr.o: fillme.h libft/libft.h /usr/include/unistd.h
sources/ft_tetr.o: /usr/include/_types.h /usr/include/sys/_types.h
sources/ft_tetr.o: /usr/include/sys/cdefs.h
sources/ft_tetr.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_tetr.o: /usr/include/sys/_posix_availability.h
sources/ft_tetr.o: /usr/include/machine/_types.h /usr/include/i386/_types.h
sources/ft_tetr.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_tetr.o: /usr/include/sys/unistd.h
sources/ft_tetr.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_tetr.o: /usr/include/sys/_types/_seek_set.h
sources/ft_tetr.o: /usr/include/sys/_types/_size_t.h
sources/ft_tetr.o: /usr/include/_types/_uint64_t.h
sources/ft_tetr.o: /usr/include/Availability.h
sources/ft_tetr.o: /usr/include/AvailabilityInternal.h
sources/ft_tetr.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_uid_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_gid_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_off_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_pid_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_null.h /usr/include/sys/select.h
sources/ft_tetr.o: /usr/include/sys/appleapiopts.h
sources/ft_tetr.o: /usr/include/sys/_types/_fd_def.h
sources/ft_tetr.o: /usr/include/sys/_types/_timespec.h
sources/ft_tetr.o: /usr/include/sys/_types/_timeval.h
sources/ft_tetr.o: /usr/include/sys/_types/_time_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_tetr.o: /usr/include/sys/_types/_fd_set.h
sources/ft_tetr.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_tetr.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_tetr.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_tetr.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_tetr.o: /usr/include/sys/_select.h
sources/ft_tetr.o: /usr/include/sys/_types/_dev_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_mode_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_tetr.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_tetr.o: /usr/include/sys/_types/_offsetof.h
sources/ft_tetr.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/ft_tetr.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/ft_tetr.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/ft_tetr.o: /usr/include/i386/signal.h
sources/ft_tetr.o: /usr/include/machine/_mcontext.h
sources/ft_tetr.o: /usr/include/i386/_mcontext.h
sources/ft_tetr.o: /usr/include/mach/i386/_structs.h
sources/ft_tetr.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_tetr.o: /usr/include/sys/_types/_ucontext.h
sources/ft_tetr.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_tetr.o: /usr/include/sys/_types/_int8_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_int16_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_int32_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_int64_t.h
sources/ft_tetr.o: /usr/include/_types/_uint8_t.h
sources/ft_tetr.o: /usr/include/_types/_uint16_t.h
sources/ft_tetr.o: /usr/include/_types/_uint32_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_tetr.o: /usr/include/_types/_intmax_t.h
sources/ft_tetr.o: /usr/include/_types/_uintmax_t.h
sources/ft_tetr.o: /usr/include/machine/endian.h /usr/include/i386/endian.h
sources/ft_tetr.o: /usr/include/sys/_endian.h
sources/ft_tetr.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_tetr.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_tetr.o: /usr/include/alloca.h /usr/include/sys/_types/_ct_rune_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_rune_t.h
sources/ft_tetr.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_tetr.o: /usr/include/inttypes.h libft/get_next_line.h
sources/ft_tetr.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/ft_tetr.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_tetr.o: /usr/include/secure/_common.h /usr/include/fcntl.h
sources/ft_tetr.o: /usr/include/sys/fcntl.h /usr/include/sys/_types/_o_sync.h
sources/ft_tetr.o: /usr/include/sys/_types/_o_dsync.h
sources/ft_tetr.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_tetr.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_tetr.o: /usr/include/sys/stat.h
sources/ft_tetr.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_ino_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_tetr.o: /usr/include/sys/types.h
sources/ft_tetr.o: /usr/include/sys/_types/_in_addr_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_in_port_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_key_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_clock_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_errno_t.h
sources/ft_tetr.o: /usr/include/sys/_pthread/_pthread_cond_t.h
sources/ft_tetr.o: /usr/include/sys/_pthread/_pthread_condattr_t.h
sources/ft_tetr.o: /usr/include/sys/_pthread/_pthread_mutex_t.h
sources/ft_tetr.o: /usr/include/sys/_pthread/_pthread_mutexattr_t.h
sources/ft_tetr.o: /usr/include/sys/_pthread/_pthread_once_t.h
sources/ft_tetr.o: /usr/include/sys/_pthread/_pthread_rwlock_t.h
sources/ft_tetr.o: /usr/include/sys/_pthread/_pthread_rwlockattr_t.h
sources/ft_tetr.o: /usr/include/sys/_pthread/_pthread_t.h
sources/ft_tetr.o: /usr/include/sys/_pthread/_pthread_key_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_fsblkcnt_t.h
sources/ft_tetr.o: /usr/include/sys/_types/_fsfilcnt_t.h
sources/ft_tetr.o: /usr/include/limits.h /usr/include/machine/limits.h
sources/ft_tetr.o: /usr/include/i386/limits.h /usr/include/i386/_limits.h
sources/ft_tetr.o: /usr/include/sys/syslimits.h
sources/ft_validate.o: fillme.h libft/libft.h /usr/include/unistd.h
sources/ft_validate.o: /usr/include/_types.h /usr/include/sys/_types.h
sources/ft_validate.o: /usr/include/sys/cdefs.h
sources/ft_validate.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_validate.o: /usr/include/sys/_posix_availability.h
sources/ft_validate.o: /usr/include/machine/_types.h
sources/ft_validate.o: /usr/include/i386/_types.h
sources/ft_validate.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_validate.o: /usr/include/sys/unistd.h
sources/ft_validate.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_validate.o: /usr/include/sys/_types/_seek_set.h
sources/ft_validate.o: /usr/include/sys/_types/_size_t.h
sources/ft_validate.o: /usr/include/_types/_uint64_t.h
sources/ft_validate.o: /usr/include/Availability.h
sources/ft_validate.o: /usr/include/AvailabilityInternal.h
sources/ft_validate.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_validate.o: /usr/include/sys/_types/_uid_t.h
sources/ft_validate.o: /usr/include/sys/_types/_gid_t.h
sources/ft_validate.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_validate.o: /usr/include/sys/_types/_off_t.h
sources/ft_validate.o: /usr/include/sys/_types/_pid_t.h
sources/ft_validate.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_validate.o: /usr/include/sys/_types/_null.h
sources/ft_validate.o: /usr/include/sys/select.h
sources/ft_validate.o: /usr/include/sys/appleapiopts.h
sources/ft_validate.o: /usr/include/sys/_types/_fd_def.h
sources/ft_validate.o: /usr/include/sys/_types/_timespec.h
sources/ft_validate.o: /usr/include/sys/_types/_timeval.h
sources/ft_validate.o: /usr/include/sys/_types/_time_t.h
sources/ft_validate.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_validate.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_validate.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_validate.o: /usr/include/sys/_types/_fd_set.h
sources/ft_validate.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_validate.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_validate.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_validate.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_validate.o: /usr/include/sys/_select.h
sources/ft_validate.o: /usr/include/sys/_types/_dev_t.h
sources/ft_validate.o: /usr/include/sys/_types/_mode_t.h
sources/ft_validate.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_validate.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_validate.o: /usr/include/sys/_types/_offsetof.h
sources/ft_validate.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_validate.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_validate.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_validate.o: /usr/include/sys/_types/_wint_t.h
sources/ft_validate.o: /usr/include/stdlib.h /usr/include/sys/wait.h
sources/ft_validate.o: /usr/include/sys/_types/_id_t.h
sources/ft_validate.o: /usr/include/sys/signal.h
sources/ft_validate.o: /usr/include/machine/signal.h
sources/ft_validate.o: /usr/include/i386/signal.h
sources/ft_validate.o: /usr/include/machine/_mcontext.h
sources/ft_validate.o: /usr/include/i386/_mcontext.h
sources/ft_validate.o: /usr/include/mach/i386/_structs.h
sources/ft_validate.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_validate.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_validate.o: /usr/include/sys/_types/_ucontext.h
sources/ft_validate.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_validate.o: /usr/include/sys/_types/_int8_t.h
sources/ft_validate.o: /usr/include/sys/_types/_int16_t.h
sources/ft_validate.o: /usr/include/sys/_types/_int32_t.h
sources/ft_validate.o: /usr/include/sys/_types/_int64_t.h
sources/ft_validate.o: /usr/include/_types/_uint8_t.h
sources/ft_validate.o: /usr/include/_types/_uint16_t.h
sources/ft_validate.o: /usr/include/_types/_uint32_t.h
sources/ft_validate.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_validate.o: /usr/include/_types/_intmax_t.h
sources/ft_validate.o: /usr/include/_types/_uintmax_t.h
sources/ft_validate.o: /usr/include/machine/endian.h
sources/ft_validate.o: /usr/include/i386/endian.h /usr/include/sys/_endian.h
sources/ft_validate.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_validate.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_validate.o: /usr/include/alloca.h
sources/ft_validate.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_validate.o: /usr/include/sys/_types/_rune_t.h
sources/ft_validate.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_validate.o: /usr/include/inttypes.h libft/get_next_line.h
sources/ft_validate.o: /usr/include/stdio.h
sources/ft_validate.o: /usr/include/sys/_types/_va_list.h
sources/ft_validate.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_validate.o: /usr/include/secure/_common.h /usr/include/fcntl.h
sources/ft_validate.o: /usr/include/sys/fcntl.h
sources/ft_validate.o: /usr/include/sys/_types/_o_sync.h
sources/ft_validate.o: /usr/include/sys/_types/_o_dsync.h
sources/ft_validate.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_validate.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_validate.o: /usr/include/sys/stat.h
sources/ft_validate.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_validate.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_validate.o: /usr/include/sys/_types/_ino_t.h
sources/ft_validate.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_validate.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_validate.o: /usr/include/sys/types.h
sources/ft_validate.o: /usr/include/sys/_types/_in_addr_t.h
sources/ft_validate.o: /usr/include/sys/_types/_in_port_t.h
sources/ft_validate.o: /usr/include/sys/_types/_key_t.h
sources/ft_validate.o: /usr/include/sys/_types/_clock_t.h
sources/ft_validate.o: /usr/include/sys/_types/_errno_t.h
sources/ft_validate.o: /usr/include/sys/_pthread/_pthread_cond_t.h
sources/ft_validate.o: /usr/include/sys/_pthread/_pthread_condattr_t.h
sources/ft_validate.o: /usr/include/sys/_pthread/_pthread_mutex_t.h
sources/ft_validate.o: /usr/include/sys/_pthread/_pthread_mutexattr_t.h
sources/ft_validate.o: /usr/include/sys/_pthread/_pthread_once_t.h
sources/ft_validate.o: /usr/include/sys/_pthread/_pthread_rwlock_t.h
sources/ft_validate.o: /usr/include/sys/_pthread/_pthread_rwlockattr_t.h
sources/ft_validate.o: /usr/include/sys/_pthread/_pthread_t.h
sources/ft_validate.o: /usr/include/sys/_pthread/_pthread_key_t.h
sources/ft_validate.o: /usr/include/sys/_types/_fsblkcnt_t.h
sources/ft_validate.o: /usr/include/sys/_types/_fsfilcnt_t.h
sources/main.o: fillme.h libft/libft.h /usr/include/unistd.h
sources/main.o: /usr/include/_types.h /usr/include/sys/_types.h
sources/main.o: /usr/include/sys/cdefs.h /usr/include/sys/_symbol_aliasing.h
sources/main.o: /usr/include/sys/_posix_availability.h
sources/main.o: /usr/include/machine/_types.h /usr/include/i386/_types.h
sources/main.o: /usr/include/sys/_pthread/_pthread_types.h
sources/main.o: /usr/include/sys/unistd.h
sources/main.o: /usr/include/sys/_types/_posix_vdisable.h
sources/main.o: /usr/include/sys/_types/_seek_set.h
sources/main.o: /usr/include/sys/_types/_size_t.h
sources/main.o: /usr/include/_types/_uint64_t.h /usr/include/Availability.h
sources/main.o: /usr/include/AvailabilityInternal.h
sources/main.o: /usr/include/sys/_types/_ssize_t.h
sources/main.o: /usr/include/sys/_types/_uid_t.h
sources/main.o: /usr/include/sys/_types/_gid_t.h
sources/main.o: /usr/include/sys/_types/_intptr_t.h
sources/main.o: /usr/include/sys/_types/_off_t.h
sources/main.o: /usr/include/sys/_types/_pid_t.h
sources/main.o: /usr/include/sys/_types/_useconds_t.h
sources/main.o: /usr/include/sys/_types/_null.h /usr/include/sys/select.h
sources/main.o: /usr/include/sys/appleapiopts.h
sources/main.o: /usr/include/sys/_types/_fd_def.h
sources/main.o: /usr/include/sys/_types/_timespec.h
sources/main.o: /usr/include/sys/_types/_timeval.h
sources/main.o: /usr/include/sys/_types/_time_t.h
sources/main.o: /usr/include/sys/_types/_suseconds_t.h
sources/main.o: /usr/include/sys/_types/_sigset_t.h
sources/main.o: /usr/include/sys/_types/_fd_setsize.h
sources/main.o: /usr/include/sys/_types/_fd_set.h
sources/main.o: /usr/include/sys/_types/_fd_clr.h
sources/main.o: /usr/include/sys/_types/_fd_isset.h
sources/main.o: /usr/include/sys/_types/_fd_zero.h
sources/main.o: /usr/include/sys/_types/_fd_copy.h /usr/include/sys/_select.h
sources/main.o: /usr/include/sys/_types/_dev_t.h
sources/main.o: /usr/include/sys/_types/_mode_t.h
sources/main.o: /usr/include/sys/_types/_uuid_t.h /usr/include/gethostuuid.h
sources/main.o: /usr/include/stddef.h /usr/include/sys/_types/_offsetof.h
sources/main.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/main.o: /usr/include/sys/_types/_rsize_t.h
sources/main.o: /usr/include/sys/_types/_wchar_t.h
sources/main.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/main.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/main.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/main.o: /usr/include/i386/signal.h /usr/include/machine/_mcontext.h
sources/main.o: /usr/include/i386/_mcontext.h
sources/main.o: /usr/include/mach/i386/_structs.h
sources/main.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/main.o: /usr/include/sys/_types/_sigaltstack.h
sources/main.o: /usr/include/sys/_types/_ucontext.h
sources/main.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/main.o: /usr/include/sys/_types/_int8_t.h
sources/main.o: /usr/include/sys/_types/_int16_t.h
sources/main.o: /usr/include/sys/_types/_int32_t.h
sources/main.o: /usr/include/sys/_types/_int64_t.h
sources/main.o: /usr/include/_types/_uint8_t.h
sources/main.o: /usr/include/_types/_uint16_t.h
sources/main.o: /usr/include/_types/_uint32_t.h
sources/main.o: /usr/include/sys/_types/_uintptr_t.h
sources/main.o: /usr/include/_types/_intmax_t.h
sources/main.o: /usr/include/_types/_uintmax_t.h
sources/main.o: /usr/include/machine/endian.h /usr/include/i386/endian.h
sources/main.o: /usr/include/sys/_endian.h
sources/main.o: /usr/include/libkern/_OSByteOrder.h
sources/main.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/main.o: /usr/include/alloca.h /usr/include/sys/_types/_ct_rune_t.h
sources/main.o: /usr/include/sys/_types/_rune_t.h
sources/main.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/main.o: /usr/include/inttypes.h libft/get_next_line.h
sources/main.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/main.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/main.o: /usr/include/secure/_common.h /usr/include/fcntl.h
sources/main.o: /usr/include/sys/fcntl.h /usr/include/sys/_types/_o_sync.h
sources/main.o: /usr/include/sys/_types/_o_dsync.h
sources/main.o: /usr/include/sys/_types/_s_ifmt.h
sources/main.o: /usr/include/sys/_types/_filesec_t.h /usr/include/sys/stat.h
sources/main.o: /usr/include/sys/_types/_blkcnt_t.h
sources/main.o: /usr/include/sys/_types/_blksize_t.h
sources/main.o: /usr/include/sys/_types/_ino_t.h
sources/main.o: /usr/include/sys/_types/_ino64_t.h
sources/main.o: /usr/include/sys/_types/_nlink_t.h /usr/include/sys/types.h
sources/main.o: /usr/include/sys/_types/_in_addr_t.h
sources/main.o: /usr/include/sys/_types/_in_port_t.h
sources/main.o: /usr/include/sys/_types/_key_t.h
sources/main.o: /usr/include/sys/_types/_clock_t.h
sources/main.o: /usr/include/sys/_types/_errno_t.h
sources/main.o: /usr/include/sys/_pthread/_pthread_cond_t.h
sources/main.o: /usr/include/sys/_pthread/_pthread_condattr_t.h
sources/main.o: /usr/include/sys/_pthread/_pthread_mutex_t.h
sources/main.o: /usr/include/sys/_pthread/_pthread_mutexattr_t.h
sources/main.o: /usr/include/sys/_pthread/_pthread_once_t.h
sources/main.o: /usr/include/sys/_pthread/_pthread_rwlock_t.h
sources/main.o: /usr/include/sys/_pthread/_pthread_rwlockattr_t.h
sources/main.o: /usr/include/sys/_pthread/_pthread_t.h
sources/main.o: /usr/include/sys/_pthread/_pthread_key_t.h
sources/main.o: /usr/include/sys/_types/_fsblkcnt_t.h
sources/main.o: /usr/include/sys/_types/_fsfilcnt_t.h
