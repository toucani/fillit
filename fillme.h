/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillme.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 12:56:54 by dkovalch          #+#    #+#             */
/*   Updated: 2019/04/13 12:14:35 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of fillit project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FILLME_H
# define FILLME_H

# include "libft.h"
# include <fcntl.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdbool.h>
# include <sys/stat.h>
# include <sys/types.h>

typedef struct		s_figure
{
	struct s_figure	*next;
	unsigned int	x_y;
	unsigned int	height;
	char			block[4][4];
	char			symbol;
}					t_figure;

typedef struct		s_tetr
{
	struct s_tetr	*next;
	struct s_figure	*parent;
	unsigned int	coordinates[4];
}					t_tetr;

char				*ft_field_create(unsigned int width);
void				ft_field_clean(char *arr, char c);
t_figure			*ft_fig_new(const char *block);
void				ft_fig_push(t_figure **list, t_figure *elem);
t_figure			*ft_fig_get(t_figure *list, unsigned int position);
unsigned int		ft_fig_count(t_figure *list);
void				ft_fig_reinit(t_figure *list);
int					ft_fig_height(const char *block);
void				ft_fig_move(t_figure *elem);
void				ft_fig_add(t_tetr *element, char *field);
void				ft_fig_remove(t_tetr *element, char *field);
void				ft_matrix_clear(t_tetr **rows);
void				ft_matrix_create(t_tetr **rows, t_figure *figures,
					const char *field);
bool				ft_position_validate(t_figure *elem, char *arr,
					unsigned int a_c);
bool				ft_position_next(t_figure *elem, char *arr);
bool				ft_solve(t_figure *list, const char *field);
void				ft_solve_puzzle(t_figure *figures);
t_tetr				*ft_tetr_new(t_figure *parent, unsigned int field_w);
void				ft_tetr_push(t_tetr **list, t_tetr *elem);
t_tetr				*ft_tetr_get(t_tetr *list, unsigned int position);
unsigned int		ft_tetr_count(t_tetr *list);
int					ft_valid_block(const char *block);
int					ft_valid_tetr(char *block);
int					ft_validate(char *filename, t_figure **list);
void				ft_usage(void);
int					open_file(char *filename);
int					ft_sqrt(int nb);

#endif
