/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_position.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/12 11:51:07 by okosiako          #+#    #+#             */
/*   Updated: 2019/04/13 12:27:11 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of fillit project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "fillme.h"

bool	ft_place_fig(t_list *elem, char *arr, unsigned int t, unsigned int y)
{
	unsigned int	x;
	unsigned int	w;
	unsigned int	a_c;

	x = 0;
	w = ft_strlen_dl(arr, '\n') + 1;
	while (x++ < 4)
	{
		while (y++ < 4)
			if (elem->block[x - 1][y - 1])
			{
				a_c = ((x - 1) * w + (y - 1)) + t;
				if (arr[a_c] == '.')
					arr[a_c] = elem->symbol;
				else
				{
					ft_field_clean(arr, elem->symbol);
					return (false);
				}
			}
		y = 0;
	}
	return (true);
}

bool	ft_check_position(t_list *elem, char *arr, unsigned int a_c)
{
	int				temp;
	unsigned int	w;
	unsigned int	x;
	unsigned int	y;

	x = 0;
	y = 0;
	w = ft_strlen_dl(arr, '\n') + 1;
	temp = 0;
	if (w - 1 - (a_c / w) < (unsigned int)elem->height)
		return (false);
	while (y < 3)
	{
		if (elem->block[x][y])
		{
			if (y > (a_c % w))
				return (false);
			temp = a_c - y;
			arr[a_c] = elem->symbol;
			y++;
			break ;
		}
		y++;
	}
	return (ft_place_fig(elem, arr, temp, y));
}

bool	ft_find_position(t_list *elem, char *arr, unsigned int a_c)
{
	unsigned int	size;
	int				w;

	w = ft_strlen_dl(arr, '\n');
	size = w * w + w;
	while (a_c < size)
	{
		if (arr[a_c] == '.')
		{
			if (!(elem->x == a_c / w && elem->y == a_c % w) &&
					ft_check_position(elem, arr, a_c))
				return (true);
			else
			{
				ft_field_clean(arr, elem->symbol);
				return (false);
			}
		}
		(a_c)++;
		if (arr[a_c] == '\n')
			(a_c)++;
	}
	return (false);
}
