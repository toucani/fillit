/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_validate.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/13 17:40:48 by dkovalch          #+#    #+#             */
/*   Updated: 2016/12/21 15:51:32 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of fillit project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "fillme.h"

/*
**Return values:
**	OK = +1
**	KO = negative
**The function checks:
**	that all block are 4x4
**	the amount of symbols
**	the amount of #
**	that symbols are only "#" and "."
*/

int		ft_valid_block(const char *block)
{
	unsigned int ct;
	unsigned int am;

	if (ft_strlen(block) < 20)
		return (-1);
	ct = 0;
	am = 0;
	while (ct++ < 20)
		if (!(ct % 5))
		{
			if (block[ct - 1] != '\n')
				return (-2);
		}
		else if (block[ct - 1] != '.' && block[ct - 1] != '#')
			return (-3);
		else if (block[ct - 1] == '#')
			am++;
	if (block[19] != '\n' || (block[20] && block[20] != '\n'))
		return (-4);
	if (am != 4)
		return (-5);
	return (1);
}

/*
**Return values:
**	OK = +1
**	KO = negative
**The function checks that:
**	there is no spaces between # and # in any row
**	there is no spaces between # and # in any column
*/

int		ft_valid_tetr(char *block)
{
	unsigned int	ct;
	int				min_con;

	if (ft_strstr(block, "#..#") || ft_strstr(block, "#.#"))
		return (-2);
	ct = 0;
	min_con = ft_fig_height(block) - 1;
	while (ct++ < 14)
		if (block[ct - 1] == '#' && block[ct + 4] == '#')
			min_con--;
	return ((min_con <= 0) ? 1 : -3);
}

/*
**Main validating function
**Return values:
**	-1 = open error
**	-2 = current block in invalid
**	-3 = current tetrimino is invalid
**	-4 = too many tetrimino
**	-5 = '\n' before EOF
*/

int		ft_validate(char *filename, t_figure **list)
{
	int		fd;
	int		temp;
	char	*block;
	int		flag;

	block = ft_strnew(21);
	if ((fd = open_file(filename)) < 0)
		return (-1);
	flag = 0;
	while ((temp = read(fd, block, 21)))
	{
		if (ft_valid_block(block) <= 0)
			return (-2);
		if (ft_valid_tetr(block) <= 0)
			return (-3);
		ft_fig_push(list, ft_fig_new(block));
		if (temp == 20)
			flag = 1;
		if (ft_fig_get(*list, ft_fig_count(*list) - 1)->symbol > 'Z')
			return (-4);
	}
	close(fd);
	return ((flag) ? 1 : -5);
}
