/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_matrix.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/21 17:15:09 by dkovalch          #+#    #+#             */
/*   Updated: 2016/12/26 10:35:03 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of fillit project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "fillme.h"
#include <limits.h>

void	ft_matrix_clear(t_tetr **rows)
{
	t_tetr	*temp;

	if (!(*rows))
		return ;
	while ((*rows)->next)
	{
		temp = ft_tetr_get(*rows, ft_tetr_count(*rows) - 1);
		free(temp->next);
		temp->next = 0;
	}
	free(*rows);
	*rows = 0;
}

void	ft_matrix_create(t_tetr **rows, t_figure *figure, const char *field)
{
	unsigned int	w;

	w = ft_strlen_dl(field, '\n') + 1;
	ft_matrix_clear(rows);
	if (figure)
	{
		while (ft_position_next(figure, (char*)field))
			ft_tetr_push(rows, ft_tetr_new(figure, w));
		figure->x_y = UINT_MAX;
	}
}
