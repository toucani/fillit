/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_solve.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/15 14:58:00 by dkovalch          #+#    #+#             */
/*   Updated: 2019/04/13 12:24:29 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of fillit project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "fillme.h"

static unsigned int g_am = 0;

bool	ft_solve(t_figure *list, const char *field)
{
	t_tetr		*matrix;

	matrix = 0;
	g_am++;
	if (g_am >= 500)
		return (false);
	if (!list)
		return (true);
	ft_matrix_create(&matrix, list, field);
	if (!matrix)
		return (false);
	ft_fig_add(matrix, (char*)field);
	while (!ft_solve(list->next, field))
	{
		ft_fig_remove(matrix, (char*)field);
		if (!matrix->next)
			return (false);
		matrix = matrix->next;
		ft_fig_add(matrix, (char*)field);
	}
	return (true);
}

/*
**	Main solving function - recursive for different field size
*/

void	ft_solve_puzzle(t_figure *figures)
{
	char			*field;
	unsigned int	width;

	if (!(field = ft_field_create(ft_sqrt(ft_fig_count(figures) * 4))))
		return ;
	while (!ft_solve(figures, field))
	{
		g_am = 0;
		width = ft_strlen_dl(field, '\n');
		free(field);
		if (!(field = ft_field_create(width + 1)))
			return ;
	}
	ft_putstr(field);
}
