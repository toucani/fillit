/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_position.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: okosiako <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/12 11:51:07 by okosiako          #+#    #+#             */
/*   Updated: 2019/04/13 12:26:55 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of fillit project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "fillme.h"

bool	ft_position_validate(t_figure *elem, char *arr, unsigned int a_c)
{
	int				temp;
	unsigned int	x;
	unsigned int	w;

	x = -1;
	w = ft_strlen_dl(arr, '\n') + 1;
	if (w - 1 - (a_c / w) < (unsigned int)elem->height)
		return (false);
	while (++x < 3)
		if (elem->block[0][x])
		{
			if (x > (a_c % w))
				return (false);
			temp = a_c - x;
			break ;
		}
	x = -1;
	while (++x < 16)
		if (elem->block[x / 4][x % 4])
		{
			if (!(arr[(x / 4) * w + (x % 4) + temp] == '.'))
				return (false);
			arr[(x / 4) * w + (x % 4) + temp] = '*';
		}
	return (true);
}

bool	ft_position_next(t_figure *elem, char *arr)
{
	unsigned int	size;
	unsigned int	a_c;
	int				w;

	w = ft_strlen_dl(arr, '\n');
	size = w * (w + 1);
	a_c = (elem->x_y > size) ? 0 : (elem->x_y + 1);
	while (a_c < size)
	{
		if (arr[a_c] == '.')
		{
			if (ft_position_validate(elem, arr, a_c))
			{
				elem->x_y = a_c;
				ft_field_clean(arr, '*');
				return (true);
			}
			else
				ft_field_clean(arr, '*');
		}
		a_c += (arr[a_c + 1] == '\n') ? 2 : 1;
	}
	return (false);
}
