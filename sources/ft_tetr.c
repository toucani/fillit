/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tetr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/19 08:34:26 by dkovalch          #+#    #+#             */
/*   Updated: 2016/12/21 16:28:07 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of fillit project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "fillme.h"
#include <limits.h>

t_tetr			*ft_tetr_new(t_figure *parent, unsigned int field_w)
{
	t_tetr			*elem;
	int				ct;
	int				temp;
	unsigned int	ct2;

	if (!parent || !(elem = (t_tetr*)malloc(sizeof(t_tetr))))
		return (0);
	ft_bzero(elem, sizeof(elem));
	elem->next = 0;
	elem->parent = parent;
	ct = -1;
	ct2 = 0;
	temp = 0;
	if (!parent->block[0][0] && !parent->block[0][1])
		temp = -2;
	else if (!parent->block[0][0])
		temp = -1;
	while (++ct < 16)
		if (parent->block[ct / 4][ct % 4])
			elem->coordinates[ct2++] = parent->x_y +
				((ct / 4) * field_w) + ct % 4 + temp;
	return (elem);
}

void			ft_tetr_push(t_tetr **list, t_tetr *elem)
{
	t_tetr	*temp;

	if (!(*list))
		*list = elem;
	else
	{
		temp = *list;
		while (temp->next)
			temp = temp->next;
		temp->next = elem;
	}
}

t_tetr			*ft_tetr_get(t_tetr *list, unsigned int position)
{
	if (position > ft_tetr_count(list))
		return (0);
	while (--position)
		list = list->next;
	return (list);
}

unsigned int	ft_tetr_count(t_tetr *list)
{
	if (list)
		return (1 + ft_tetr_count(list->next));
	return (0);
}
