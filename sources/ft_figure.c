/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_figure.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/15 12:36:19 by dkovalch          #+#    #+#             */
/*   Updated: 2016/12/21 16:21:43 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of fillit project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "fillme.h"

/*
**Return values:
**	height of the figure(of the last # found)
**	OK - positive
**	KO - -1
*/

int		ft_fig_height(const char *block)
{
	int diff;

	diff = ft_strrchr(block, '#') - ft_strchr(block, '#');
	if (diff <= 0)
		return (-1);
	return ((diff / 4 + (((diff % 4) > 0) ? 1 : 0)));
}

/*
**Moving figure to the upper-left corner to reduce empty colums and rows
*/

void	ft_fig_move(t_figure *elem)
{
	unsigned int a;
	unsigned int x;

	a = 0;
	while (a++ < 3)
		if (!ft_isalpha(elem->block[0][0]) && !ft_isalpha(elem->block[0][1])
		&& !ft_isalpha(elem->block[0][2]) && !ft_isalpha(elem->block[0][3]))
		{
			ft_memmove((void*)(elem->block[0]), (void*)(elem->block[1]), 15);
			ft_bzero((void*)(elem->block[3]), 4);
		}
	a = 0;
	while (a++ < 3)
		if (!ft_isalpha(elem->block[0][0]) && !ft_isalpha(elem->block[1][0])
		&& !ft_isalpha(elem->block[2][0]) && !ft_isalpha(elem->block[3][0]) &&
				!(x = 0))
			while (x++ < 4)
			{
				elem->block[x - 1][0] = elem->block[x - 1][1];
				elem->block[x - 1][1] = elem->block[x - 1][2];
				elem->block[x - 1][2] = elem->block[x - 1][3];
				elem->block[x - 1][3] = 0;
			}
}

/*
**	Putting a tetrimino into the field
*/

void	ft_fig_add(t_tetr *element, char *field)
{
	field[element->coordinates[0]] = element->parent->symbol;
	field[element->coordinates[1]] = element->parent->symbol;
	field[element->coordinates[2]] = element->parent->symbol;
	field[element->coordinates[3]] = element->parent->symbol;
}

/*
**	Removing a tetrimino from the field
*/

void	ft_fig_remove(t_tetr *element, char *field)
{
	field[element->coordinates[0]] = '.';
	field[element->coordinates[1]] = '.';
	field[element->coordinates[2]] = '.';
	field[element->coordinates[3]] = '.';
}
