/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 14:41:24 by dkovalch          #+#    #+#             */
/*   Updated: 2016/12/21 15:51:08 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of fillit project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "fillme.h"

void	ft_usage(void)
{
	ft_putendl("usage: fillit source_file");
	exit(0);
}

int		open_file(char *filename)
{
	int file;

	if (!filename)
		return (0);
	file = open(filename, O_RDONLY);
	if (file >= 0)
		return (file);
	else
		return (-1);
}

int		ft_sqrt(int nb)
{
	int		x;

	if (nb <= 4)
		return (2);
	x = 3;
	while (x * x < nb)
		x++;
	return (x);
}

int		main(int ac, char **av)
{
	t_figure *figures;

	figures = 0;
	if (ac != 2)
		ft_usage();
	if (ft_validate(av[1], &figures) <= 0)
	{
		ft_putendl("error");
		exit(1);
	}
	ft_solve_puzzle(figures);
	exit(0);
}
