/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_field.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/22 16:23:50 by dkovalch          #+#    #+#             */
/*   Updated: 2016/12/22 16:23:51 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of fillit project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "fillme.h"

/*
**	Creating and initializing the field
*/

char	*ft_field_create(unsigned int width)
{
	size_t	ct;
	char	*array;

	if (!(array = ft_strnew(width * (width + 1))))
		return (0);
	ct = 0;
	while (ct++ < width * (width + 1))
		if (ct / (width + 1) && !(ct % (width + 1)))
			array[ct - 1] = '\n';
		else
			array[ct - 1] = '.';
	return (array);
}

/*
**	Cleaning the field after the experiments with tetrimino
*/

void	ft_field_clean(char *arr, char c)
{
	char *point;

	while ((point = ft_strchr(arr, c)))
		*point = '.';
}
