/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fig.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/19 08:22:53 by dkovalch          #+#    #+#             */
/*   Updated: 2016/12/21 16:21:31 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of fillit project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "fillme.h"
#include <limits.h>

t_figure		*ft_fig_new(const char *block)
{
	t_figure	*elem;
	size_t		ct;
	static char	symbol = 'A' - 1;

	if (!block || !(elem = (t_figure*)malloc(sizeof(t_figure))))
		return (0);
	ft_bzero(elem, sizeof(elem));
	elem->next = 0;
	elem->height = ft_fig_height(block);
	elem->symbol = ++symbol;
	elem->x_y = UINT_MAX;
	ct = -1;
	while ((ct += (block[ct + 1] == '\n') ? 2 : 1) < 19)
		elem->block[ct / 5][ct % 5] = (block[ct] == '.') ? 0 : symbol;
	ft_fig_move(elem);
	return (elem);
}

void			ft_fig_push(t_figure **list, t_figure *elem)
{
	t_figure	*temp;

	if (!(*list))
		*list = elem;
	else
	{
		temp = *list;
		while (temp->next)
			temp = temp->next;
		temp->next = elem;
	}
}

t_figure		*ft_fig_get(t_figure *list, unsigned int position)
{
	if (position > ft_fig_count(list))
		return (0);
	while (position--)
		list = list->next;
	return (list);
}

unsigned int	ft_fig_count(t_figure *list)
{
	if (list)
		return (1 + ft_fig_count(list->next));
	return (0);
}

void			ft_fig_reinit(t_figure *list)
{
	while (list)
	{
		list->x_y = UINT_MAX;
		list = list->next;
	}
}
