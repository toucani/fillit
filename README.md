# fillit

Fillit is a small program, which takes a file with 1 to 26 tetriminos as an argument, and places all of them into the smallest square it can.


## Installing

Clone this repository recursively
```
git clone --recurse-submodules
```
and
```
make
```

## Testing

Unzip *Fillit-test* and run ```./CheckFillit``` to see how it works. You also can run ```./Generate [tetrimino amount] > [file]``` to generate valid tetrimino map.

Feel free to take the *Fillit-test.zip* to check your friends' fillits.


## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Authors

* Olha Kosiakova
* Dmytro Kovalchuk

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=fillit from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to UNIT Factory, for inspiration to do my best.
* to all UNIT Factory students, who shared their knowledge with me and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kyiv) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).